﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Linq;
using System.Reflection;

namespace Tenax.Utilities
{
    public class StateSerializationContractResolver : DefaultContractResolver
    {
        private readonly bool _includeData = false;

        public StateSerializationContractResolver(bool includeData = false)
        {
            _includeData = includeData;
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            JsonProperty property = base.CreateProperty(member, memberSerialization);

            property.ShouldSerialize = (instance) =>
            {
                try
                {
                    // Get the property, to see if it throws an exception
                    var propertyInfo = (PropertyInfo)member;
                    if (propertyInfo.CanRead)
                    {
                        propertyInfo.GetValue(instance, null);
                    }
                }
                catch (InvalidOperationException)
                {
                    return false;
                }

                return !member.CustomAttributes.Any(x => x.AttributeType == typeof(LoadedDataAttribute)) || _includeData;
            };

            return property;
        }
    }
}
