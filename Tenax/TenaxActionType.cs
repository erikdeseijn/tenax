﻿namespace Tenax
{
    public enum TenaxActionType
    {
        /// <summary>
        /// Action method is Action<TState>. It's completed unless an exception occurs.
        /// </summary>
        Regular = 1,

        /// <summary>
        /// Action method is Func<TState, bool>. It's completed when it returns true, or not when false or an exception occurs.
        /// </summary>
        Boolean = 2,

        /// <summary>
        /// Action method is async Func<TState, Task>. It's completed unless an exception occurs.
        /// </summary>
        Async = 3,

        /// <summary>
        /// Action method is async Func<TState, Task<bool>>. It's completed when it returns true, or not when false or an exception occurs.
        /// </summary>
        AsyncBoolean = 4
    }
}
