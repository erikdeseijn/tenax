﻿using System;

namespace Tenax
{
    [Flags]
    public enum TenaxTracingLevel
    {
        None = 0,
        Exceptions = 1,
        Actions = 2,
        States = 4, // BEWARE: Loaded data can easily be megabytes in size!
        
        Default = 3,
        All = 7,
    }
}
