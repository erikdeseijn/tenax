﻿using System;
using System.Collections.Generic;

namespace Tenax.Example
{
    /// <summary>
    /// A faux database for demonstration purposes
    /// </summary>
    public class Database : IDatabase
    {
        public Dictionary<Guid, object> Stored { get; set; } = new Dictionary<Guid, object>();

        public T Select<T>(Guid id) where T : class
        {
            if (Stored.ContainsKey(id))
            {
                return (T)Stored[id];
            }
            throw new Exception($"Item of type {typeof(T)} with id {id} does not exist");
        }

        public void Insert<T>(Guid id, T record) where T : class
        {
            if (Stored.ContainsKey(id))
            {
                throw new Exception($"Item of type {typeof(T)} with id {id} already exists");
            }
            Stored.Add(id, record);
        }

        public void Update<T>(Guid id, T record) where T : class
        {
            if (!Stored.ContainsKey(id))
            {
                throw new Exception($"Item of type {typeof(T)} with id {id} does not exist");
            }
            Stored[id] = record;
        }
    }
}
