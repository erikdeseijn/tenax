# TENAX - Run tasks until completion #

Tenacious - Characterized by extreme persistence; relentless or enduring
[From Latin tenāx, tenāc-, holding fast, from tenēre, to hold;]

### What is this repository for? ###
Most operations are trivial: a user enters data and some changes are made to the database. Sometimes though, they're comprised of several separate actions, that can all easily fail. For example: the user's status is updated to Active, a Twitter account is created, a PDF document stating his Twitter account details is generated and subsequently mailed. But what if Twitter is down, the PDF's template contains errors or the user's mailbox is full?

Tenax is designed to keep trying until every action in a task is completed successfully. Generally, you will run the task's first few simple actions, like updating the status in the database on the executing thread, after which the task is queued. Execution is then transfered to a background service, which will dequeue the task's state and pick up where it left off. Over and over, until every action has been successfully executed.

### Benefits: ###
* Every task will be fully completed, even if it takes debugging
* A user will not have to wait for external system calls or long running tasks that are not required for him to proceed
* Tasks can be nested, so their code can be reused
* Logs are extremely readable
* Development is sped up (dangerous claim, I know), because "only one" test case needs to be set up and Tenax will drag you along, until every action is implemented and debugged.

### Prerequisites ###
* A system capable of running .NET Standard 2.1, C# 8.0
* A queue for running background tasks
* .NET Core 3.1 to run the examples and tests

### Set up ###
* Install the latest version through the Nuget package manager.
* Define your task class, interface and state
* Configure your dependency injection framework
* Write code to call your task when its state is dequeued

### Terminology ###
Some terminology has already been used, let's clearify before we continue:

Term | Meaning
------------- | -------------
Task | A class that contains all actions to complete the task
Action | A method on a Task class that represents a single subtask
State | A serializable object that contains all information on the task's progress
Unqueued execution | The first (indispensible) part of the task is run on the executing thread, after which the state is queued for queued execution
Queued execution | The state is taken off the queue by a background service and executed there

### Usage ###

There are basically four things to do:

* Define the state
* Write the task definition and action methods
* Write code to queue the state
* Write code to dequeue the state and execute the task again

I've provided an example project, so you should be able to learn how it's done by reading the comments and code (-:

### Recommendations ###

A few tips to make your life easier:

* Don't requeue a task with a fixed short interval, like every minute. If it fails, it will probably keep failing. Instead, execute it after 1, 2, 4, 8, 16, etc minutes. This way it'll remain queued, but won't drastically hurt server performance while you're debugging it.
* Build a page that shows the currently queued tasks and logs, especially if you want to demonstrate that things have in fact improved.

### To do ###

A few improvements can be made:

* Since actions generally use the database, or do a REST-request, the overhead of task initialization is not immediately worth minimizing. But it can be done, by keeping initialized tasks in memory.
* Adding a small queue, that can be used if you don't have one and don't want to write or implement one.
* It's possible to execute only part of a task by setting one or more actions as completed in the state beforehand. A state generator could be built to help with that.