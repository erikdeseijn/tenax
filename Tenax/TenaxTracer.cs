﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;
using Tenax.Utilities;

namespace Tenax
{
    internal class TenaxTracer
    {
        internal TenaxTracingLevel Level { get; set; } = TenaxTracingLevel.Default;
        internal string Prefix { get; set; } = string.Empty;
        internal readonly List<string> Lines = new List<string>();

        private string _currentAction;
        private DateTime _currentStartDate;
        private string _currentState;

        internal TenaxTracer()
        {
        }

        internal void StartExecution(Type classType)
        {
            if (Level.HasFlag(TenaxTracingLevel.Actions))
            { 
                string prefix = Prefix;
                if (prefix != string.Empty)
                {
                    prefix = prefix[0..^2] + "- ";
                }
                Lines.Add($"{prefix}Task: {classType.Name}");
            }
        }

        internal void StartAction(string methodName)
        {
            if (Level.HasFlag(TenaxTracingLevel.Actions))
            { 
                _currentAction = methodName;            
                _currentStartDate = DateTime.UtcNow;
                _currentState = null;
            }
        }

        internal void AddState(TenaxState state)
        {
            if (Level.HasFlag(TenaxTracingLevel.States))
            { 
                _currentState = state.Serialize(true);
            }
        }

        internal void EndAction(string result)
        {
            if (Level.HasFlag(TenaxTracingLevel.Actions))
            {
                Lines.Add($"{Prefix}|- Action: {_currentAction}");
                Lines.Add($"{Prefix}|  |- Date: {_currentStartDate:yyyy-MM-dd HH:mm:ss:fff}");
                if (_currentState != null)
                {
                    Lines.Add($"{Prefix}|  |- State: {_currentState}");
                }
                Lines.Add($"{Prefix}|  |- Ran for: {DateTime.UtcNow.Subtract(_currentStartDate).TotalMilliseconds.ToString("N2", CultureInfo.InvariantCulture)} ms");
                Lines.Add($"{Prefix}|  +- Result: {result}");
            }
            else if (Level.HasFlag(TenaxTracingLevel.States))
            {
                Lines.Add(_currentState);
            }
        }

        internal void EndActionWithException(Exception ex)
        {
            if (Level.HasFlag(TenaxTracingLevel.Actions))
            {
                string[] lines = Regex.Split(ex.GetFullStackTrace(), Environment.NewLine);

                for (int i = 0; i < lines.Length; i++)
                {
                    if (i == 0)
                    {
                        lines[0] = $"Exception \"{lines[0]}\"";
                    }
                    else
                    {
                        lines[i] = $"{Prefix}|        {lines[i].Trim()}";
                    }
                }

                EndAction(string.Join(Environment.NewLine, lines));
            }
            else if (Level.HasFlag(TenaxTracingLevel.Exceptions))
            {
                Lines.Add(ex.GetFullStackTrace());
            }
        }

        internal void AddCompleted()
        {
            if (Level.HasFlag(TenaxTracingLevel.Actions))
            { 
                EndAction("Complete");
            }
        }

        internal void AddNotCompleted()
        {
            if (Level.HasFlag(TenaxTracingLevel.Actions))
            {
                EndAction("Not completed");
            }
        }

        internal void AddContinueOnError()
        {
            if (Level.HasFlag(TenaxTracingLevel.Actions))
            {
                Lines.Add($"{Prefix}|- Continue on error");
            }
        }

        internal void AddStoppedUnqueued()
        {
            if (Level.HasFlag(TenaxTracingLevel.Actions))
            {
                Lines.Add($"{Prefix}|- Stopped before queued actions while running unqueued");
            }
        }

        internal void EndActionWithTracer(TenaxTracer tracer)
        {
            foreach (var line in tracer.Lines)
            {
                Lines.Add($"{Prefix}{line}");
            }
        }

        public override string ToString()
        {
            return string.Join(Environment.NewLine, Lines);
        }

        internal void EndExecution(bool complete)
        {
            if (Level.HasFlag(TenaxTracingLevel.Actions))
            {
                string result = complete ? "Complete" : "Wait for retry";
                Lines.Add($"{Prefix}+- Result: {result}");
            }
        }
    }
}
