﻿using System;
using Tenax.Utilities;

namespace Tenax.Example.TenaxTasks.GenerateInvoice
{
    public class GenerateInvoiceState : TenaxState
    {
        // To generate the PDF, we only need the InvoiceId
        public Guid InvoiceId { get; set; }

        /// <summary>
        /// The Invoice object with all the required information is loaded each time
        /// the task is executed, so it is marked as LoadedData. It will not be serialized.
        /// </summary>
        [LoadedData]
        public Invoice Invoice { get; set; }
    }
}
