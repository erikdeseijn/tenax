﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Tenax.Example.TenaxTasks.ApproveOrder;
using Tenax.Example.TenaxTasks.GenerateInvoice;

namespace Tenax.Example
{
    class Program
    {
        static void Main()
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IDatabase, Database>()
                .AddTransient<IApproveOrderTask, ApproveOrderTask>()
                .AddTransient<IGenerateInvoiceTask, GenerateInvoiceTask>()
                .BuildServiceProvider();

            var examples = new List<Func<IServiceProvider, Task>>
            {
                EntirelyQueuedExample,
                PartiallyQueuedExample
            };

            int i = 1;
            ConsoleWriteLine($"Run example:", ConsoleColor.Yellow);
            foreach (var example in examples)
            {
                ConsoleWriteLine($"{i++} {example.GetMethodInfo().Name}");
            }
            var key = Console.ReadKey(true);
            try
            {
                Console.Clear();

                int index = (byte)key.KeyChar - 48 - 1;
                Task.Run(async () => await examples[index].Invoke(serviceProvider)).Wait();
            }
            catch
            {
                ConsoleWriteLine("Wrong input");
            }
        }

        private async static Task EntirelyQueuedExample(IServiceProvider serviceProvider)
        {
            ConsoleWriteLine("Run task queued entirely");

            var database = serviceProvider.GetService<IDatabase>();
            var queue = new Queue<object>();

            // Create and store an order that needs approval
            var order = new Order
            {
                Id = Guid.NewGuid(),
                Amount = 123.45m,
                Customer = "Erik",
                Status = Order.OrderStatus.Pending
            };
            database.Insert(order.Id, order);
            
            // Enqueue a task to approve the order
            queue.Enqueue(new ApproveOrderState
            {
                OrderId = order.Id
            });

            // Pretend to move to back end and start processing the queue
            await RunQueue(serviceProvider, queue);

            Console.ReadKey(true);
        }

        private async static Task PartiallyQueuedExample(IServiceProvider serviceProvider)
        {
            ConsoleWriteLine("Run task unqueued partially");

            var database = serviceProvider.GetService<IDatabase>();
            var queue = new Queue<object>();

            // Create and store an order that needs approval
            var order = new Order
            {
                Id = Guid.NewGuid(),
                Amount = 123.45m,
                Customer = "Erik",
                Status = Order.OrderStatus.Pending
            };
            database.Insert(order.Id, order);

            // Create a task to approve the order
            var unqueuedState = new ApproveOrderState
            {
                OrderId = order.Id
            };

            try
            {
                // Run the first part (updating the order status, so the customer can continue) 
                // unqueued. It must succeed to continue to the queued part of the task.
                var unqueuedJob = serviceProvider.GetService<IApproveOrderTask>();
                await unqueuedJob.ExecuteUnqueuedAsync(unqueuedState).ConfigureAwait(false);

                // Enqueue to run on back end
                queue.Enqueue(unqueuedState);

                // Pretend to move to back end and start processing the queue
                ConsoleWriteLine(">>> Task run from queue");
                await RunQueue(serviceProvider, queue);
            }
            catch (Exception ex)
            {
                ConsoleWriteLine($"Fatal exception: {ex.Message}", ConsoleColor.Red);
            }

            Console.ReadKey(true);
        }

        private async static Task RunQueue(IServiceProvider serviceProvider, Queue<object> queue)
        {
            while (queue.TryDequeue(out object item))
            {
                ConsoleWriteLine($"Queued item {item.GetType().Name} found", ConsoleColor.Yellow);

                if (item is ApproveOrderState queuedState)
                {
                    // Execute the second part queued
                    var queuedJob = serviceProvider.GetService<IApproveOrderTask>();
                    await queuedJob.ExecuteAsync(queuedState).ConfigureAwait(false);

                    if (!queuedJob.IsComplete)
                    {
                        string trace = queuedJob.GetTrace();
                        queue.Enqueue(queuedState);

                        ConsoleWriteLine("Trace:", ConsoleColor.Red);
                        ConsoleWriteLine(trace, ConsoleColor.DarkRed);
                        ConsoleWriteLine("Job not complete. Requeueing state");
                    }
                }
            }

            ConsoleWriteLine("Queue is empty. Task complete", ConsoleColor.Yellow);
        }

        /// <summary>
        /// Ask if the current piece of code should succeed or fail. This is of course not
        /// how it's done, but this example is not meant to teach how inversion of control
        /// works. This is just easier.
        /// Even though it's used as if it can return false, it always fails with an exception.
        /// </summary>
        public static bool AskForOutcome([CallerFilePath] string path = null, [CallerMemberName] string name = null)
        {
            ConsoleWriteLine($"{Path.GetFileNameWithoutExtension(path)}.{name}", ConsoleColor.Cyan);
            ConsoleWriteLine("S: Success");
            ConsoleWriteLine("E: Error");
            while (true)
            {
                var key = Console.ReadKey(true).Key;

                if (key == ConsoleKey.S)
                {
                    return true;
                }
                else if (key == ConsoleKey.E)
                {
                    throw new Exception($"'{name}' failed");
                }
            }
        }

        private static void ConsoleWriteLine(string line, ConsoleColor color = ConsoleColor.Gray)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(line);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
