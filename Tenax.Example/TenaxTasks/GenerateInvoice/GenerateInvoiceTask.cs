﻿using System;
using System.Threading.Tasks;

namespace Tenax.Example.TenaxTasks.GenerateInvoice
{
    /// <summary>
    /// This task generates and stores the invoice document. Generation could be an HTML to PDF 
    /// converter and storage might be handled by a microservice. 
    /// 
    /// The task inherits TenaxTask<> with the state as its generic argument. To be able to use
    /// it with dependecy injection, we'll write an interface as well.
    /// </summary>
    public class GenerateInvoiceTask : TenaxTask<GenerateInvoiceState>, IGenerateInvoiceTask
    {
        // IDatabase is a service injected by DI. IServiceProvider is mandatory, but it's 
        // possible to add other services. 
        // Note that anything related to data, will not get properties in the task class.
        // Those should always be kept in the state.
        private readonly IDatabase _database;

        // The constructor handles DI.
        public GenerateInvoiceTask(IServiceProvider serviceProvider, IDatabase database) : base(serviceProvider)
        {
            _database = database;
        }

        /// <summary>
        /// Define the task's actions. We're provided with a task builder.
        /// </summary>
        public override void Define(TenaxTaskBuilder<GenerateInvoiceState> builder)
        {
            // The first action loads the Invoice object from the database.
            builder.Action(LoadData, (s) =>
            {
                // This action should always be run, because only the InvoiceId is 
                // stored in the state.
                s.RunAlways();
                // When loaded, the next action is document generation.
                s.AddNextAction(GenerateDocument);
            });

            // The second action generates the PDF
            builder.Action(GenerateDocument, (s) =>
            {
                // As the PDF is stored on the file server through a microservice, 
                // the PDF should also be generated each time.
                s.RunAlways();
                // Next, store the document.
                s.AddNextAction(StoreDocument);
            });

            // The final action stores the document. It requires no additional setup.
            builder.Action(StoreDocument);
        }

        public bool LoadData(GenerateInvoiceState state)
        {
            if (Program.AskForOutcome())
            {
                if (state.Invoice == null)
                {
                    state.Invoice = _database.Select<Invoice>(state.InvoiceId);

                    Console.WriteLine($"Invoice {state.InvoiceId} loaded");
                }

                return true;
            }
            return false;
        }

        public async Task<bool> GenerateDocument(GenerateInvoiceState state)
        {
            // This is a long running task, so it's going to be async. As we don't actually
            // generate a PDF in this example, let's avoid the warning by awaiting some delay.
            await Task.Delay(100);
            if (Program.AskForOutcome())
            {
                state.Invoice.Document = 
$@"Invoice
Number: {state.Invoice.Id}
To: {state.Invoice.Customer}
Amount: {state.Invoice.Amount}";

                Console.WriteLine("Document generated");
                return true;
            }
            return false;
        }

        public bool StoreDocument(GenerateInvoiceState state)
        {
            if (Program.AskForOutcome())
            {
                _database.Update(state.Invoice.Id, state.Invoice);

                Console.WriteLine("Document stored");
                return true;
            }
            return false;
        }
    }
}
