﻿using System;
using System.Threading.Tasks;

namespace Tenax
{
    public class TenaxActionBuilder<TState> where TState : TenaxState
    {
        private readonly TenaxAction<TState> _action;
        
        public TenaxActionBuilder(TenaxAction<TState> action)
        {
            _action = action;
        }

        /// <summary>
        /// Specify consecutive actions.
        /// </summary>
        public TenaxActionBuilder<TState> AddNextAction(Action<TState> action)
        {
            _action.NextActions.Add(action.Method.Name);
            return this;
        }

        /// <summary>
        /// Specify consecutive actions.
        /// </summary>
        public TenaxActionBuilder<TState> AddNextAction(Func<TState, bool> action)
        {
            _action.NextActions.Add(action.Method.Name);
            return this;
        }

        /// <summary>
        /// Specify consecutive actions.
        /// </summary>
        public TenaxActionBuilder<TState> AddNextAction(Func<TState, Task> action)
        {
            _action.NextActions.Add(action.Method.Name);
            return this;
        }

        /// <summary>
        /// Specify consecutive actions.
        /// </summary>
        public TenaxActionBuilder<TState> AddNextAction(Func<TState, Task<bool>> action)
        {
            _action.NextActions.Add(action.Method.Name);
            return this;
        }

        /// <summary>
        /// Specify consecutive actions.
        /// </summary>
        public TenaxActionBuilder<TState> AddNextAction<TNestedTask>()
        {
            _action.NextActions.Add(typeof(TNestedTask).Name);
            return this;
        }

        /// <summary>
        /// Continue running consecutive actions if this method fails.
        /// </summary>
        public TenaxActionBuilder<TState> ContinueOnError()
        {
            _action.ContinueOnError = true;
            return this;
        }

        /// <summary>
        /// Specifies that this action can run in an unqueued first execution of the task.
        /// </summary>
        public TenaxActionBuilder<TState> CanRunUnqueued()
        {
            _action.CanRunUnqueued = true;
            return this;
        }

        /// <summary>
        /// This method will always be run. Use it to for example load from a database.
        /// </summary>
        public TenaxActionBuilder<TState> RunAlways()
        {
            _action.RunAlways = true;
            return this;
        }
    }
}
