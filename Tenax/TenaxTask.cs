﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tenax.Utilities;

namespace Tenax
{
    public abstract class TenaxTask<TState> : ITenaxTask<TState>
        where TState : TenaxState
    {
        public IServiceProvider ServiceProvider { get; set; }
        internal TenaxTracer Tracer { get; set; } = new TenaxTracer();

        internal List<TenaxAction<TState>> Actions { get; set; } = new List<TenaxAction<TState>>();
        private readonly ConcurrentQueue<TenaxAction<TState>> _actionQueue = new ConcurrentQueue<TenaxAction<TState>>();
        internal bool RunningUnqueued = false;

        public TenaxTask(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
            Define(new TenaxTaskBuilder<TState>(this));
            ValidateDefinition();
        }

        /// <summary>
        /// Return the results of the tracer as a string
        /// </summary>
        public string GetTrace()
        {
            return Tracer.ToString();
        }

        /// <summary>
        /// Create the definition of the task by specifying actions.
        /// </summary>
        public abstract void Define(TenaxTaskBuilder<TState> builder);

        /// <summary>
        /// Validates the definition and throws an exception with all errors.
        /// </summary>
        protected void ValidateDefinition()
        {
            var errors = new List<string>();

            if (Actions.Count() > 0)
            {
                var actionsReached = new List<string> { Actions.First().Name };

                foreach (var action in Actions)
                {
                    Type actionType = action.GetType();
                    bool actionRunsQueuedOnly = !action.CanRunUnqueued;

                    if (actionType.IsSubclassOfRawGeneric(typeof(TenaxNestedTaskAction<,,>)))
                    {
                        if (!actionType.GenericTypeArguments[1].IsInterface)
                        {
                            errors.Add(TenaxValidationException.NestedTaskTypeMustBeInterface(actionType.GenericTypeArguments[1].Name));
                        }
                    }

                    foreach (var nextActionName in action.NextActions)
                    {
                        actionsReached.Add(nextActionName);

                        if (!Actions.Any(x => x.Name == nextActionName))
                        {
                            errors.Add(TenaxValidationException.UndefinedAction(nextActionName));
                        }
                        else
                        {
                            var nextAction = Actions.Single(x => x.Name == nextActionName);
                            if (actionRunsQueuedOnly && nextAction.CanRunUnqueued)
                            {
                                errors.Add(TenaxValidationException.UnqueuedActionAfterQueuedAction(nextAction.Name, action.Name));
                            }
                        }
                    }
                }

                foreach (var action in Actions)
                {
                    if (!actionsReached.Contains(action.Name))
                    {
                        errors.Add(TenaxValidationException.UnreachedAction(action.Name));
                    }
                }
            }

            if (errors.Count > 0)
            {
                throw new TenaxValidationException(errors);
            }
        }

        /// <summary>
        /// Returns whether or not the task was fully completed.
        /// </summary>
        public bool IsComplete { get; internal set; }

        /// <summary>
        /// Run unqueued (in executing thread) and stop when no next action can run unqueued.
        /// </summary>
        /// <param name="state">The state to be executed</param>
        public async Task ExecuteUnqueuedAsync(TState state)
        {
            try
            {
                RunningUnqueued = true;
                await ExecuteAsync(state).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                IsComplete = false;
                throw ex;
            }
        }

        /// <summary>
        /// Run the task from queue.
        /// </summary>
        /// <param name="state">The state to be executed</param>
        public async Task ExecuteAsync(TState state)
        {
            Tracer.StartExecution(GetType());

            IsComplete = true;
            _actionQueue.Enqueue(Actions.First());

            while (_actionQueue.TryDequeue(out TenaxAction<TState> action))
            {
                if (!RunningUnqueued || action.CanRunUnqueued)
                {
                    await action.ExecuteAsync(state).ConfigureAwait(false);

                    if (state.IsActionComplete(action.Name) || action.ContinueOnError)
                    {
                        // Add next actions to the queue
                        foreach (var nextAction in action.NextActions)
                        {
                            _actionQueue.Enqueue(Actions.Single(x => x.Name == nextAction));
                        }
                    }
                }
                else
                {
                    IsComplete = false;
                }
            }

            // Stop unqueued execution if a queued method is encountered
            if (RunningUnqueued && !IsComplete)
            {
                RunningUnqueued = false;
                IsComplete = false;
                Tracer.AddStoppedUnqueued();
            }

            Tracer.EndExecution(IsComplete);
        }
    }
}
