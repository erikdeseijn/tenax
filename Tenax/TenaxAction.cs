﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Tenax
{
    public class TenaxAction<TState> where TState : TenaxState
    {
        protected TenaxTask<TState> Task;
        public virtual string Name { get; private set; }
        public TenaxActionType ActionType { get; private set; }
        public List<string> NextActions { get; set; } = new List<string>();

        public Action<TState> RegularMethod { get; set; }
        public Func<TState, bool> BooleanMethod { get; set; }
        public Func<TState, Task> AsyncMethod { get; set; }
        public Func<TState, Task<bool>> AsyncBooleanMethod { get; set; }
        
        public bool RunAlways { get; internal set; } = false;
        public bool ContinueOnError { get; internal set; } = false;
        public bool CanRunUnqueued { get; internal set; } = false;

        protected TenaxAction(TenaxTask<TState> task)
        {
            Task = task;
        }

        public TenaxAction(TenaxTask<TState> task, Action<TState> method)
            : this(task)
        {
            ActionType = TenaxActionType.Regular;
            RegularMethod = method;
            Name = method.Method.Name;
        }

        public TenaxAction(TenaxTask<TState> task, Func<TState, bool> method)
            : this(task)
        {
            ActionType = TenaxActionType.Boolean;
            BooleanMethod = method;
            Name = method.Method.Name;
        }

        public TenaxAction(TenaxTask<TState> task, Func<TState, Task> method)
            : this(task)
        {
            ActionType = TenaxActionType.Async;
            AsyncMethod = method;
            Name = method.Method.Name;
        }

        public TenaxAction(TenaxTask<TState> task, Func<TState, Task<bool>> method)
            : this(task)
        {
            ActionType = TenaxActionType.AsyncBoolean;
            AsyncBooleanMethod = method;
            Name = method.Method.Name;
        }

        /// <summary>
        /// Execute the action if not yet completed or if it should always run
        /// </summary>
        public virtual async Task ExecuteAsync(TState state)
        {
            if (RunAlways || !state.IsActionComplete(Name))
            {
                Task.Tracer.StartAction(Name);
                Task.Tracer.AddState(state);

                try
                {
                    state.SetActionComplete(Name, false);

                    bool successful = false;
                    switch (ActionType)
                    {
                        case TenaxActionType.Regular:
                            RegularMethod.Invoke(state);
                            successful = true;
                            break;
                        case TenaxActionType.Boolean:
                            successful = BooleanMethod.Invoke(state);
                            break;
                        case TenaxActionType.Async:
                            await AsyncMethod.Invoke(state).ConfigureAwait(false);
                            successful = true;
                            break;
                        case TenaxActionType.AsyncBoolean:
                            successful = await AsyncBooleanMethod.Invoke(state).ConfigureAwait(false);
                            break;
                    }

                    state.SetActionComplete(Name, successful);

                    if (successful)
                    {
                        Task.Tracer.AddCompleted();
                    }
                    else
                    {
                        Task.Tracer.AddNotCompleted();
                        Task.IsComplete = false;
                    }
                }
                catch (Exception ex)
                {
                    Task.IsComplete = false;
                    Task.Tracer.EndActionWithException(ex);

                    // When running unqueued (so without queued retries), actually throw exceptions
                    if (Task.RunningUnqueued)
                    {
                        Task.Tracer.EndExecution(false);
                        throw;
                    }

                    if (ContinueOnError)
                    {
                        Task.Tracer.AddContinueOnError();
                    }
                }
            }
        }
    }
}
