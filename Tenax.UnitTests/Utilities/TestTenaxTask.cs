﻿using System;
using System.Threading.Tasks;

namespace Tenax.UnitTests.Utilities
{
    public class TestState : TenaxState
    {
        public int Counter1 = 0;
        public TestState NestedTestState
        {
            get => new TestState();
        }
    }

    public class TestTenaxTask : TenaxTask<TestState>, ITestTenaxTask
    {
        public TestTenaxTask(
            IServiceProvider serviceProvider,
            Action<TestTenaxTask, TenaxTaskBuilder<TestState>> definition) : base(serviceProvider)
        {
            definition.Invoke(this, new TenaxTaskBuilder<TestState>(this));
            ValidateDefinition();
        }

        public override void Define(TenaxTaskBuilder<TestState> builder)
        {
        }

        public bool ActionComplete1(TestState state)
        {
            return true;
        }

        public bool ActionComplete2(TestState state)
        {
            return true;
        }

        public bool ActionNotComplete1(TestState state)
        {
            return false;
        }

        public void ActionRunAlways1(TestState state)
        {
            state.Counter1++;
        }

        public async Task<bool> AsyncActionComplete1Async(TestState state)
        {
            await Task.Delay(0);
            return true;
        }

        public void ThrowsException(TestState state)
        {
            throw new Exception("Exception in action");
        }
    }
}
