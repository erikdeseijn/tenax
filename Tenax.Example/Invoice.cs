﻿using System;

namespace Tenax.Example
{
    public class Invoice
    {
        public Guid Id { get; set; }
        public Guid OrderId { get; set; }
        public string Customer { get; set; }
        public decimal Amount { get; set; }
        public string Document { get; set; }
    }
}
