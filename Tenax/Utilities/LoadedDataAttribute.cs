﻿using System;

namespace Tenax.Utilities
{
    /// <summary>
    /// This field is loaded data and should only be serialized when tracing.
    /// </summary>
    public class LoadedDataAttribute : Attribute
    {
    }
}
