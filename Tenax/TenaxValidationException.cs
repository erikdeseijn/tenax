﻿using System;
using System.Collections.Generic;

namespace Tenax
{
    public class TenaxValidationException : Exception
    {
        public IEnumerable<string> Errors { get; }

        public TenaxValidationException(IEnumerable<string> errors) : base("Validation errors found")
        {
            Errors = errors;
        }

        public static string NestedTaskTypeMustBeInterface(string typeName) 
            => $"Nested task type {typeName} must be an interface";

        public static string UndefinedAction(string actionName)
            => $"{actionName} not defined as action";

        public static string UnqueuedActionAfterQueuedAction(string nextActionName, string actionName)
            => $"Unqueued action {nextActionName} defined after queued action {actionName}";

        public static string UnreachedAction(string actionName)
            => $"Action {actionName} will never be reached";
    }
}
