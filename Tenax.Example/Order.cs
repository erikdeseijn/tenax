﻿using System;

namespace Tenax.Example
{
    public class Order
    {
        public enum OrderStatus
        {
            Pending = 1,
            Rejected = 2,
            Approved = 3
        }

        public Guid Id { get; set; }
        public OrderStatus Status { get; set; } = OrderStatus.Pending;
        public string Customer { get; set; }
        public decimal Amount { get; set; }
    }
}
