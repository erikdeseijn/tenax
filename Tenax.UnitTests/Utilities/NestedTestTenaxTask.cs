﻿using System;

namespace Tenax.UnitTests.Utilities
{
    public class NestedTestTenaxTask : TestTenaxTask, INestedTestTenaxTask
    {
        public NestedTestTenaxTask(
            IServiceProvider serviceProvider,
            Action<TestTenaxTask, TenaxTaskBuilder<TestState>> definition) : base(serviceProvider, definition)
        {
        }
    }
}
