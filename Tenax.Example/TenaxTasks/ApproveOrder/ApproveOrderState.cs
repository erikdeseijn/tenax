﻿using System;
using Tenax.Example.TenaxTasks.GenerateInvoice;
using Tenax.Utilities;

namespace Tenax.Example.TenaxTasks.ApproveOrder
{
    public class ApproveOrderState : TenaxState
    {
        // The ID of the order to be approved.
        public Guid OrderId { get; set; }

        // The ID of the invoice that is created in this task.
        public Guid? InvoiceId { get; set; }

        // Generating the invoice document is a nested task. Upon instantiation,
        // it will receive the invoice ID from this state.
        private GenerateInvoiceState _generateInvoiceState;
        public GenerateInvoiceState GenerateInvoiceState
        {
            get
            {
                if (_generateInvoiceState == null && InvoiceId.HasValue)
                {
                    _generateInvoiceState = new GenerateInvoiceState
                    {
                        InvoiceId = InvoiceId.Value,
                        Invoice = Invoice
                    };
                }
                return _generateInvoiceState;
            }
            set { _generateInvoiceState = value; }
        }

        /// <summary>
        /// The Order object with all the required information is loaded each time
        /// the task is executed, so it is marked as LoadedData. It will not be serialized.
        /// </summary>
        [LoadedData]
        public Order Order { get; set; }

        /// <summary>
        /// The Invoice object with all the required information is loaded each time
        /// the task is executed, so it is marked as LoadedData. It will not be serialized.
        /// </summary>
        [LoadedData]
        public Invoice Invoice { get; set; }
    }
}
