﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Tenax
{
    public class TenaxTaskBuilder<TState> where TState : TenaxState
    {
        private readonly TenaxTask<TState> _task;

        public TenaxTaskBuilder(TenaxTask<TState> task)
        {
            _task = task;
        }

        /// <summary>
        /// Define an action.
        /// </summary>
        /// <param name="method">The method that is called for this action</param>
        /// <param name="configuration">Configuration of the action with an action builder</param>
        public TenaxTaskBuilder<TState> Action(
            Action<TState> method,
            Action<TenaxActionBuilder<TState>> configuration = null)
        {
            var action = new TenaxAction<TState>(_task, method);
            _task.Actions.Add(action);
            configuration?.Invoke(new TenaxActionBuilder<TState>(action));
            return this;
        }

        /// <summary>
        /// Define an action.
        /// </summary>
        /// <param name="booleanMethod">The method that is called for this action</param>
        /// <param name="configuration">Configuration of the action with an action builder</param>
        public TenaxTaskBuilder<TState> Action(
            Func<TState, bool> booleanMethod,
            Action<TenaxActionBuilder<TState>> configuration = null)
        {
            var action = new TenaxAction<TState>(_task, booleanMethod);
            _task.Actions.Add(action);
            configuration?.Invoke(new TenaxActionBuilder<TState>(action));
            return this;
        }

        /// <summary>
        /// Define an action.
        /// </summary>
        /// <param name="method">The method that is called for this action</param>
        /// <param name="configuration">Configuration of the action with an action builder</param>
        public TenaxTaskBuilder<TState> Action(
            Func<TState, Task> asyncMethod,
            Action<TenaxActionBuilder<TState>> configuration = null)
        {
            var action = new TenaxAction<TState>(_task, asyncMethod);
            _task.Actions.Add(action);
            configuration?.Invoke(new TenaxActionBuilder<TState>(action));
            return this;
        }

        /// <summary>
        /// Define an action.
        /// </summary>
        /// <param name="method">The method that is called for this action</param>
        /// <param name="configuration">Configuration of the action with an action builder</param>
        public TenaxTaskBuilder<TState> Action(
            Func<TState, Task<bool>> asyncBooleanMethod,
            Action<TenaxActionBuilder<TState>> configuration = null)
        {
            var action = new TenaxAction<TState>(_task, asyncBooleanMethod);
            _task.Actions.Add(action);
            configuration?.Invoke(new TenaxActionBuilder<TState>(action));
            return this;
        }

        /// <summary>
        /// Define another, nested task as an action
        /// </summary>
        /// <typeparam name="TNestedTask">Type of the nested task (use interface type for DI!)</typeparam>
        /// <typeparam name="TNestedState">Type of the nested state</typeparam>
        /// <param name="createNestedStateExpression">Expression pointing to the nested state on the state</param>
        /// <param name="configuration">Configure the action</param>
        /// <returns></returns>
        public TenaxTaskBuilder<TState> NestedTask<TNestedTask, TNestedState>(
            Expression<Func<TState, TNestedState>> createNestedStateExpression,
            Action<TenaxActionBuilder<TState>> configuration = null)
            where TNestedTask : ITenaxTask<TNestedState>
            where TNestedState : TenaxState
        {
            var action = new TenaxNestedTaskAction<TState, TNestedTask, TNestedState>(
                _task, 
                createNestedStateExpression);
            _task.Actions.Add(action);
            configuration?.Invoke(new TenaxActionBuilder<TState>(action));
            return this;
        }

        /// <summary>
        /// Set the level of tracing.
        /// </summary>
        public TenaxTaskBuilder<TState> TracingLevel(TenaxTracingLevel tracingLevel)
        {
            _task.Tracer.Level = tracingLevel;
            return this;
        }
    }
}
