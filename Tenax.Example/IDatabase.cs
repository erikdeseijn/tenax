﻿using System;

namespace Tenax.Example
{
    public interface IDatabase
    {
        T Select<T>(Guid id) where T : class;
        void Insert<T>(Guid id, T record) where T : class;
        void Update<T>(Guid id, T record) where T : class;
    }
}
