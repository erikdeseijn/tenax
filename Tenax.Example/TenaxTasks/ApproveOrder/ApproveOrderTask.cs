﻿using System;
using System.Threading.Tasks;
using Tenax.Example.TenaxTasks.GenerateInvoice;

namespace Tenax.Example.TenaxTasks.ApproveOrder
{
    /// <summary>
    /// Say that we run a system that handles payments for external webshops (merchants). 
    /// At a certain point the order will be approved using this task, which has the 
    /// following actions:
    /// - Update order status
    /// - Send a postback to the merchant's API
    /// - Create an invoice
    /// - Generate an invoice document
    /// - Store the invoice document
    /// - Mail the invoice to the customer
    /// Loading the order and updating the status should be done immediately, so these actions
    /// will run unqueued. The user doesn't have to wait for the rest, so the state will be 
    /// serialized and put on a queue. A background task will dequeue it and execute the 
    /// remaining actions.
    /// 
    /// The task inherits TenaxTask<> with the state as its generic argument. To be able to use
    /// it with dependecy injection, we'll write an interface as well.
    /// </summary>
    public class ApproveOrderTask : TenaxTask<ApproveOrderState>, IApproveOrderTask
    {
        // IDatabase is a service injected by DI. IServiceProvider is mandatory, but it's 
        // possible to add other services. 
        // Note that anything related to data, will not get properties in the task class.
        // Those should always be kept in the state.
        private readonly IDatabase _database;

        // The constructor handles DI.
        public ApproveOrderTask(IServiceProvider serviceProvider, IDatabase database) : base(serviceProvider)
        {
            _database = database;
        }

        /// <summary>
        /// Define the task's actions. We're provided with a task builder.
        /// </summary>
        public override void Define(TenaxTaskBuilder<ApproveOrderState> builder)
        {
            // We set the tracing level to all, which includes the serialized state. 
            // This lets you check state changes between actions. This can be useful
            // when debugging (or this example), but can also lead to megabytes of
            // logs, so we don't generally use it in a production environment. The
            // default logs actions and exceptions only.
            builder.TracingLevel(TenaxTracingLevel.All);

            // The first action loads the Order object from the database.
            builder.Action(LoadData, (a) =>
            {
                // This action should always be run, because only the OrderId is 
                // stored in the state.
                a.RunAlways();
                // This action is part of the unqueued execution
                a.CanRunUnqueued();
                // Proceed to updating the order (status)
                a.AddNextAction(UpdateOrder);
            });

            // Then update the order. This will change the order status.
            builder.Action(UpdateOrder, (a) =>
            {
                // This action is part of the unqueued execution
                a.CanRunUnqueued();
                // Proceed to sending a postback to the merchant's API...
                a.AddNextAction(SendMerchantPostback);
                // ... and creating the invoice
                a.AddNextAction(CreateInvoice);
            });

            // Send a postback to the merchant's API
            builder.Action(SendMerchantPostback, (a) =>
            {
                // When this action fails, it should not halt execution
                a.ContinueOnError();
            });

            // Create the invoice object.
            builder.Action(CreateInvoice, (a) =>
            {
                // Continue to generate the invoice's PDF.
                a.AddNextAction<IGenerateInvoiceTask>();
            });

            // Generate and store the invoice's PDF. This is a nested task. Its state
            // is stored in this task's state, so we point to the nested state property.
            builder.NestedTask<IGenerateInvoiceTask, GenerateInvoiceState>(x => x.GenerateInvoiceState, (a) =>
            {
                // Continue to mailing the invoice
                a.AddNextAction(EmailInvoice);
            });

            // Finally, mail the invoice to the customer.
            builder.Action(EmailInvoice);
        }

        public async Task<bool> LoadData(ApproveOrderState state)
        {
            await Task.Delay(0);
            if (Program.AskForOutcome())
            {
                // Maybe this task is nested in another task, which set
                // the object when creating the state. Only load it when
                // empty.
                if (state.Order == null)
                {
                    state.Order = _database.Select<Order>(state.OrderId);
                    Console.WriteLine($"Order {state.OrderId} loaded");
                }

                // At first execution, the invoice doesn't yet exist. When
                // it's created and the task should run a second time, load
                // it, rather then create a second one.
                if (state.Invoice == null && state.InvoiceId.HasValue)
                {
                    state.Order = _database.Select<Order>(state.InvoiceId.Value);
                    Console.WriteLine($"Invoice {state.InvoiceId} loaded");
                }

                return true;
            }
            return false;
        }

        // All database actions should of course run asynchronously, but this 
        // is also an example, so I'll take the liberty of mixing in some other 
        // action types.
        public bool UpdateOrder(ApproveOrderState state)
        {
            if (Program.AskForOutcome())
            {
                state.Order.Status = Order.OrderStatus.Approved;
                _database.Update(state.Order.Id, state.Order);

                Console.WriteLine($"Order {state.OrderId} updated");
                return true;
            }
            return false;
        }

        public void CreateInvoice(ApproveOrderState state)
        {
            Program.AskForOutcome();
            
            state.Invoice = new Invoice
            {
                Id = Guid.NewGuid(),
                OrderId = state.Order.Id,
                Amount = state.Order.Amount,
                Customer = state.Order.Customer
            };
            _database.Insert(state.Invoice.Id, state.Invoice);

            // Store the generated invoice ID in the state, so it can be 
            // loaded next time the task is executed.
            state.InvoiceId = state.Invoice.Id;

            Console.WriteLine($"Invoice {state.InvoiceId} inserted");
        }

        public async Task SendMerchantPostback(ApproveOrderState state)
        {
            await Task.Delay(0);
            Program.AskForOutcome();
            Console.WriteLine($"Merchant postback succeeded");
        }

        public bool EmailInvoice(ApproveOrderState state)
        {
            if (Program.AskForOutcome())
            {
                Console.WriteLine($"Invoice {state.InvoiceId} e-mailed");
                return true;
            }
            return false;
        }
    }
}
