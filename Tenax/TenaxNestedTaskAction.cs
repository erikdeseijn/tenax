﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tenax.Utilities;

namespace Tenax
{
    /// <summary>
    /// An action that runs a nested task
    /// </summary>
    public class TenaxNestedTaskAction<TState, TNestedTask, TNestedState> : TenaxAction<TState>
        where TState : TenaxState
        where TNestedState : TenaxState
    {
        public override string Name => typeof(TNestedTask).Name;
        private readonly Func<TState, TNestedState> _createNestedStateDelegate;

        public TenaxNestedTaskAction(TenaxTask<TState> task, Expression<Func<TState, TNestedState>> createNestedStateExpression)
            : base(task)
        {
            _createNestedStateDelegate = createNestedStateExpression.Compile();
        }

        /// <summary>
        /// Instead of running a method, execute an entire task
        /// </summary>
        public async override Task ExecuteAsync(TState state)
        {
            if (RunAlways || !state.IsActionComplete(Name))
            {
                Task.Tracer.StartAction(Name);
                Task.Tracer.AddState(state);

                try
                {
                    state.SetActionComplete(Name, false);

                    var nestedState = _createNestedStateDelegate.Invoke(state);

                    var nested = Task.ServiceProvider.GetTypedService<TNestedTask>() as TenaxTask<TNestedState>;
                    nested.Tracer.Level = Task.Tracer.Level;
                    nested.Tracer.Prefix = $"|  {Task.Tracer.Prefix}";
                    if (Task.RunningUnqueued)
                    {
                        await nested.ExecuteUnqueuedAsync(nestedState).ConfigureAwait(false);
                    }
                    else
                    { 
                        await nested.ExecuteAsync(nestedState).ConfigureAwait(false);
                    }

                    Task.Tracer.EndActionWithTracer(nested.Tracer);

                    if (nested.IsComplete)
                    {
                        state.SetActionComplete(Name);
                    }
                    else
                    {
                        Task.IsComplete = false;
                    }
                }
                catch (Exception ex)
                {
                    Task.IsComplete = false;
                    Task.Tracer.EndActionWithException(ex);

                    // When running unqueued (so without queued retries), actually throw exceptions
                    if (Task.RunningUnqueued)
                    {
                        Task.Tracer.EndExecution(false);
                        throw ex;
                    }

                    if (ContinueOnError)
                    {
                        Task.Tracer.AddContinueOnError();
                    }
                }
            }
        }
    }
}
