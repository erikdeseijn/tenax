﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Tenax.UnitTests.Utilities
{
    public class TestBase
    {
        protected readonly IServiceProvider ServiceProvider;
        protected INestedTestTenaxTask NestedTask;

        public TestBase()
        {
            ServiceProvider = new ServiceCollection()
                .AddTransient<INestedTestTenaxTask>((serviceProvider) =>
                {
                    return NestedTask;
                })
                .BuildServiceProvider();
        }
    }
}
