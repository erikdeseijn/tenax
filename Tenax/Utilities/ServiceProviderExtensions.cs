﻿using System;

namespace Tenax.Utilities
{
    public static class ServiceProviderExtensions
    {
        public static T GetTypedService<T>(this IServiceProvider serviceProvider)
        {
            return (T)serviceProvider.GetService(typeof(T));
        }
    }
}
