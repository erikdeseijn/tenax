﻿using System.Threading.Tasks;

namespace Tenax
{
    public interface ITenaxTask
    {
        bool IsComplete { get; }
        string GetTrace();
    }

    public interface ITenaxTask<TState> : ITenaxTask
    {
        Task ExecuteAsync(TState state);
        Task ExecuteUnqueuedAsync(TState state);
    }
}
