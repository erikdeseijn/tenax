﻿using System;

namespace Tenax.Utilities
{
    public static class TypeExtensions
    {
        public static bool IsSubclassOfRawGeneric(this Type type, Type genericType)
        {
            while (type != null && type != typeof(object))
            {
                var current = type.IsGenericType ? type.GetGenericTypeDefinition() : type;
                if (genericType == current)
                {
                    return true;
                }
                type = type.BaseType;
            }
            return false;
        }
    }
}
