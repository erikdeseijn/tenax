﻿using System;
using System.Threading.Tasks;
using Tenax.UnitTests.Utilities;
using Xunit;

namespace Tenax.UnitTests.Tenax
{
    public class TenaxTaskTests : TestBase
    {
        #region Validation

        [Fact]
        public void Validation_ReportsUnreachedAction()
        {
            var state = new TestState();
            TestTenaxTask uow;

            var exception = Assert.Throws<TenaxValidationException>(() =>
            {
                uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.ActionComplete1);
                    builder.Action(instance.ActionComplete2);
                });
            });

            Assert.Contains(TenaxValidationException.UnreachedAction(nameof(TestTenaxTask.ActionComplete2)), exception.Errors);
        }

        [Fact]
        public void Validation_UndefinedAction()
        {
            var state = new TestState();
            TestTenaxTask uow;

            var exception = Assert.Throws<TenaxValidationException>(() =>
            {
                uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.ActionComplete1, (a) =>
                    {
                        a.AddNextAction(instance.ActionComplete2);
                    });
                });
            });

            Assert.Contains(TenaxValidationException.UndefinedAction(nameof(TestTenaxTask.ActionComplete2)), exception.Errors);
        }

        [Fact]
        public void Validation_UnexpectedUnqueuedAction()
        {
            var state = new TestState();
            TestTenaxTask uow;

            var exception = Assert.Throws<TenaxValidationException>(() =>
            {
                uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.ActionComplete1, (a) =>
                    {
                        a.AddNextAction(instance.ActionComplete2);
                    });

                    builder.Action(instance.ActionComplete2, (a) =>
                    {
                        a.CanRunUnqueued();
                    });
                });
            });

            Assert.Contains(TenaxValidationException.UnqueuedActionAfterQueuedAction(nameof(TestTenaxTask.ActionComplete2), nameof(TestTenaxTask.ActionComplete1)), exception.Errors);
        }

        [Fact]
        public void Validation_NestedTaskNotAnInterface()
        {
            var state = new TestState();
            TestTenaxTask uow;

            var exception = Assert.Throws<TenaxValidationException>(() =>
            {
                uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.NestedTask<NestedTestTenaxTask, TestState>(x => x.NestedTestState);
                });
            });

            Assert.Contains(TenaxValidationException.NestedTaskTypeMustBeInterface(nameof(NestedTestTenaxTask)), exception.Errors);
        }

        #endregion

        #region ContinueOnError

        [Fact]
        public void ContinueOnException()
        {
            var state = new TestState();
            var uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.ThrowsException, (a) =>
                    {
                        a.ContinueOnError();
                        a.AddNextAction(instance.ActionComplete1);
                    });
                    builder.Action(instance.ActionComplete1);
                });
            
            Task.Run(async () => await uow.ExecuteAsync(state)).Wait();

            Assert.False(uow.IsComplete);
            Assert.False(state.IsActionComplete(nameof(TestTenaxTask.ThrowsException)));
            Assert.True(state.IsActionComplete(nameof(TestTenaxTask.ActionComplete1)));
        }

        [Fact]
        public void ContinueOnActionNotComplete()
        {
            var state = new TestState();
            var uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.ActionNotComplete1, (a) =>
                    {
                        a.ContinueOnError();
                        a.AddNextAction(instance.ActionComplete2);
                    });
                    builder.Action(instance.ActionComplete2);
                });

            Task.Run(async () => await uow.ExecuteAsync(state)).Wait();

            Assert.False(uow.IsComplete);
            Assert.False(state.IsActionComplete(nameof(TestTenaxTask.ActionNotComplete1)));
            Assert.True(state.IsActionComplete(nameof(TestTenaxTask.ActionComplete2)));
        }

        #endregion

        [Fact]
        public void StopsOnActionNotComplete()
        {
            var state = new TestState();
            var uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.ActionNotComplete1, (a) =>
                    {
                        a.AddNextAction(instance.ActionComplete2);
                    });
                    builder.Action(instance.ActionComplete2);
                });

            Task.Run(async () => await uow.ExecuteAsync(state)).Wait();

            Assert.False(uow.IsComplete);
            Assert.False(state.IsActionComplete(nameof(TestTenaxTask.ActionNotComplete1)));
            Assert.False(state.IsActionComplete(nameof(TestTenaxTask.ActionComplete2)));
        }

        [Fact]
        public void RunAlways()
        {
            var state = new TestState();
            var uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.ActionRunAlways1, (a) =>
                    {
                        a.RunAlways();
                        a.AddNextAction(instance.ActionNotComplete1);
                    });
                    builder.Action(instance.ActionNotComplete1);
                });

            Task.Run(async () => await uow.ExecuteAsync(state)).Wait();

            Assert.Equal(1, state.Counter1);

            Task.Run(async () => await uow.ExecuteAsync(state)).Wait();

            Assert.Equal(2, state.Counter1);
        }

        [Fact]
        public void IncompleteNestedTaskResultsInIncomplete()
        {
            NestedTask = new NestedTestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.ThrowsException);
                });

            var state = new TestState();
            var uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.NestedTask<INestedTestTenaxTask, TestState>(x => x.NestedTestState);
                });

            Task.Run(async () => await uow.ExecuteAsync(state)).Wait();

            Assert.False(uow.IsComplete);
        }

        [Fact]
        public void ExecuteUnqueuedStopsAtFirstQueued()
        {
            var state = new TestState();
            var uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.ActionComplete1, (a) =>
                    {
                        a.CanRunUnqueued();
                        a.AddNextAction(instance.ActionComplete2);
                    });
                    builder.Action(instance.ActionComplete2);
                });

            Task.Run(async () => await uow.ExecuteUnqueuedAsync(state)).Wait();

            Assert.False(uow.IsComplete);
            Assert.True(state.IsActionComplete(nameof(TestTenaxTask.ActionComplete1)));
            Assert.False(state.IsActionComplete(nameof(TestTenaxTask.ActionComplete2)));
        }

        [Fact]
        public void UnqueuedExecutionThrowsExceptions()
        {
            var state = new TestState();
            var uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.ThrowsException, (a) =>
                    {
                        a.CanRunUnqueued();
                    });
                });

            var exception = Assert.ThrowsAsync<Exception>(async () => await uow.ExecuteUnqueuedAsync(state)).Result;

            Assert.Contains("Exception in action", exception.Message);
        }

        [Fact]
        public void QueuedActionsRunAtSecondExecution()
        {
            var state = new TestState();
            var uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.ActionComplete1, (a) =>
                    {
                        a.CanRunUnqueued();
                        a.AddNextAction(instance.ActionComplete2);
                    });
                    builder.Action(instance.ActionComplete2);
                });

            Task.Run(async () => await uow.ExecuteUnqueuedAsync(state)).Wait();

            Assert.False(uow.IsComplete);
            Assert.True(state.IsActionComplete(nameof(TestTenaxTask.ActionComplete1)));
            Assert.False(state.IsActionComplete(nameof(TestTenaxTask.ActionComplete2)));

            Task.Run(async () => await uow.ExecuteAsync(state)).Wait();

            Assert.True(uow.IsComplete);
            Assert.True(state.IsActionComplete(nameof(TestTenaxTask.ActionComplete1)));
            Assert.True(state.IsActionComplete(nameof(TestTenaxTask.ActionComplete2)));
        }

        [Fact]
        public void ExecutesAsyncActions()
        {
            var state = new TestState();
            var uow = new TestTenaxTask(
                ServiceProvider,
                (instance, builder) =>
                {
                    builder.Action(instance.AsyncActionComplete1Async);
                });

            Task.Run(async () => await uow.ExecuteAsync(state)).Wait();

            Assert.True(uow.IsComplete);
        }
    }
}
