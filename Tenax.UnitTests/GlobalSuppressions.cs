﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:Tenax.UnitTests.Utilities.TestTenaxTask.ActionComplete1(Tenax.UnitTests.Utilities.TestState)~System.Boolean")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:Tenax.UnitTests.Utilities.TestTenaxTask.ActionComplete2(Tenax.UnitTests.Utilities.TestState)~System.Boolean")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:Tenax.UnitTests.Utilities.TestTenaxTask.ActionNotComplete1(Tenax.UnitTests.Utilities.TestState)~System.Boolean")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:Tenax.UnitTests.Utilities.TestTenaxTask.AsyncActionComplete1Async(Tenax.UnitTests.Utilities.TestState)~System.Threading.Tasks.Task{System.Boolean}")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:Tenax.UnitTests.Utilities.TestTenaxTask.ThrowsException(Tenax.UnitTests.Utilities.TestState)")]
