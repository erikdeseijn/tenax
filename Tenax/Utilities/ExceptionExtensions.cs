﻿using System;
using System.Text;

namespace Tenax.Utilities
{
    public static class ExceptionExtensions
    {
        public static string GetFullStackTrace(this Exception exception)
        {
            var sb = new StringBuilder();

            while (exception != null)
            {
                sb.AppendLine(exception.Message);
                sb.AppendLine();
                sb.AppendLine(exception.StackTrace);
                sb.AppendLine();

                exception = exception.InnerException;
            }

            return sb.ToString().Trim();
        }
    }
}
