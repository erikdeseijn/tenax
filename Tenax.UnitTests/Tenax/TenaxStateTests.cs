using System.Collections.Generic;
using Tenax.Utilities;
using Xunit;

namespace Tenax.UnitTests.Tenax
{
    public class TenaxStateTests
    {
        public class State : TenaxState
        {
            public List<string> StateData { get; set; }

            [LoadedData]
            public List<string> LoadedData { get; set; }
        }

        [Fact]
        public void SetActionSetActionComplete_AddsAction()
        {
            var state = new State();

            state.SetActionComplete("Test1");
            state.SetActionComplete("Test2", true);

            Assert.Contains(state.CompletedActions, x => x == "Test1");
            Assert.Contains(state.CompletedActions, x => x == "Test2");
        }

        [Fact]
        public void SetActionSetActionComplete_RemovesAction()
        {
            var state = new State();
            state.SetActionComplete("Test1");
            state.SetActionComplete("Test2");

            state.SetActionComplete("Test1", false);

            Assert.DoesNotContain(state.CompletedActions, x => x == "Test1");
            Assert.Contains(state.CompletedActions, x => x == "Test2");
        }

        [Fact]
        public void IsActionSetActionComplete_ReturnsTrue()
        {
            var state = new State();
            state.SetActionComplete("Test1");

            Assert.True(state.IsActionComplete("Test1"));
        }

        [Fact]
        public void IsActionSetActionComplete_ReturnsFalse()
        {
            var state = new State();
            state.SetActionComplete("Test1");

            Assert.False(state.IsActionComplete("Test2"));
        }

        [Fact]
        public void Serialize_WithLoadedData()
        {
            var state = new State
            {
                StateData = new List<string> { "State1", "State2" },
                LoadedData = new List<string> { "Loaded1", "Loaded2" }
            };

            string json = state.Serialize(true);

            Assert.Contains("Loaded1", json);
        }

        [Fact]
        public void Serialize_WithoutLoadedData()
        {
            var state = new State()
            {
                StateData = new List<string> { "State1", "State2" },
                LoadedData = new List<string> { "Loaded1", "Loaded2" }
            };

            string json = state.Serialize(false);

            Assert.DoesNotContain("Loaded1", json);
        }
    }
}
