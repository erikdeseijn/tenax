﻿using System;
using System.Collections.Generic;

namespace Tenax.UnitTests.Utilities
{
    public enum ActionResult
    {
        Complete,
        CompleteNotSet,
        Exception
    }

    public class ActionResultProvider
    {
        private readonly Dictionary<string, ActionResult> _results = new Dictionary<string, ActionResult>();
        private readonly Dictionary<string, Exception> _exceptions = new Dictionary<string, Exception>();

        public void AddResult(string actionName, ActionResult result, Exception exception = null)
        {
            _results.Add(actionName, result);
            if (exception != null)
            {
                _exceptions.Add(actionName, exception);
            }
        }

        public bool GetResult(string actionName)
        {
            return (_results[actionName]) switch
            {
                ActionResult.Complete => true,
                ActionResult.CompleteNotSet => false,
                _ => throw _exceptions[actionName],
            };
        }
    }
}
