﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Tenax.Utilities;

namespace Tenax
{
    [Serializable]
    public abstract class TenaxState
    {
        public List<string> CompletedActions { get; set; } = new List<string>();

        /// <summary>
        /// Check if an action was already completed.
        /// </summary>
        public bool IsActionComplete(string actionName)
        {
            return CompletedActions.Contains(actionName);
        }

        /// <summary>
        /// Add an action to the list of completed actions.
        /// </summary>
        public void SetActionComplete(string actionName, bool isComplete = true)
        {
            if (isComplete && !CompletedActions.Contains(actionName))
            {
                CompletedActions.Add(actionName);
            }
            else if (!isComplete)
            {
                CompletedActions.Remove(actionName);
            }
        }

        /// <summary>
        /// Serialize this state to JSON.
        /// </summary>
        /// <param name="includeData">Serialize with data (for error tracing) when true, or for queueing when false</param>
        public string Serialize(bool includeData)
        {
            return JsonConvert.SerializeObject(
                this, 
                new JsonSerializerSettings
                {
                    ContractResolver = new StateSerializationContractResolver(includeData)
                });
        }
    }
}
